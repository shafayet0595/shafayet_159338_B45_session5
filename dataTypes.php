<?php
// this is boolean True
$decision =TRUE;
if($decision){
    echo "decision is YES <br>";
}

// this is boolean False
$decision =false;
if($decision){
    echo "decision is YES ";
}

// integer example start here
$var1 =100; //int
$var2 = 55.35; //float

//string example here
$string1 ='123 $var1';
$string2 ="abc# $var1";
$string3 ="abc4";

echo "$string1 <br> $string2 <br> <br>";

$heredocString =<<<BITM
    heredoc line1 $var1 <br>
    heredoc line2 $var2 <br>
    heredoc line3 $string1 <br>
    heredoc line4 $var1 <br>
BITM;

$nowdocString =<<<BITM
    nowdoc line1 $var1 <br>
    nowdoc line2 $var2 <br>
    nowdoc line3 $string3 <br>
BITM;

echo $heredocString ."<br>";
echo $nowdocString ."<br>";


//Array examples start here

$ageArray = array(1,2,3,4,5,'10'=>4);
print_r(($ageArray)); echo"<br>";
echo implode($ageArray)."<br>";

$CarArray = array("ford","bmw",3,4.3,"lamborgini");
print_r($CarArray);

$ageArray = array("karim"=>30,"ronaldo"=>29,"neymar"=>"23");
print_r($ageArray);
echo "<br>";
echo $ageArray['neymar']."<br>";
$ageArray['neymar']=20;
echo"<pre>";
print_r($ageArray);
echo"</pre>.<br>";