<?php
$str ="is your name O'reilly ";
echo addslashes($str);
echo "<br>";

//implode
$arr = array("html","css", "php", "mysql");
$str = implode('" ,"',$arr);
$str ='"'.$str.'"';
echo $str;
echo"<br>";

//explode
$myStr = "Rohim Korim Jamal";
$myArr = explode(" ",$myStr);
echo "<pre>";
print_r($myArr);
echo "</pre>";

//htrmlentities
$myTutorial = '<br> means line break';
echo htmlentities($myTutorial);
echo "<br>";
echo "<br>";
//trim
$username =" rahim ";
echo $username;
echo "<br>";
$username = trim($username);
echo $username."User";
echo "<br>";
//Newline vs Break
$myNewline = "line1\nline2\nline3\n";
$myNewline = nl2br($myNewline);
echo $myNewline;

//str_pad(string,length,pad_string,pad_type)

$myString = "Hello Mars! How's it there?";
echo str_pad($myString,"35","*#");
echo"<br>";
echo str_repeat($myString,5);
echo"<br>";
//str_replace(find,replace,string,count)
echo str_replace("Mars","world","$myString");
echo "<br>";
$newArr = str_split($myString,5);
echo"<pre>";
print_r($newArr);
echo"<pre>";
//substr_compare(string1,string2,startpos,length,case)
$mainStr ="I am Shafayet!";
$str ="I am Shafayet! 2eeasda23123";
echo strpos($mainStr,"am")."<br>";
echo substr_compare($mainStr,$str,0);
